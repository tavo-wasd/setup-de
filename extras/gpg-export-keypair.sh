#!/bin/sh

[ -z "$1" ] && echo "Usage: export-gpg-keys <key ID>" && exit

echo "Will create a new directory './exported-keypair/' and then write output files there. Press ENTER to continue."
read -r NULL

mkdir -p ./exported-keypair

gpg --export "$1" > ./exported-keypair/"$1".pub
gpg --export-secret-key "$1" > ./exported-keypair/"$1"
