#!/bin/sh

[ -z "$3" ] && echo "Usage: import-gpg-keys <publickey> <privatekey> <ID>" && exit

gpg --import "$1"
gpg --allow-secret-key-import --import "$2"
gpg --edit-key "$3"
