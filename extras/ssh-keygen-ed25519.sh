#!/bin/sh

# openssh is not installed
! [ -e "/usr/bin/ssh-keygen" ] && printf "Missing /usr/bin/ssh-keygen" && exit

# Keypair already generated
if [ -e "/home/$(whoami)/.ssh/id_ed25519.pub" ] ; then
    echo
    cat ~/.ssh/id_ed25519.pub
    printf "There's already an ED25519 ssh keypair in ~/.ssh/"
    exit
fi

mkdir -p ~/.ssh
ssh-keygen -q -t ed25519 -f ~/.ssh/id_ed25519 -N ''
echo
cat ~/.ssh/id_ed25519.pub
