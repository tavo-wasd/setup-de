#!/bin/sh

# Add publickey
wget -qO /etc/apt/trusted.gpg.d/tavo@tavo.one.asc pubkey.tavo.one

# Enable repository
echo "deb [arch=amd64,all] https://gitlab.com/tavo-wasd/debian-tavo-repo/-/raw/main stable main" |
    tee /etc/apt/sources.list.d/debian-tavo-repo.list

# Update repository index
apt update

# Install desktop-tavo package
apt install desktop-tavo -y
