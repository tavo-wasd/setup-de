#!/bin/sh
# Custom Archlinux setup script
# Save script's directory and current user

SCRIPTDIR=$(cd $(dirname $0) && pwd)
usr=$(whoami)

# This is where execution starts, exit if running on privileged shell
[ "$usr" = "root" ] &&
    echo "WARNING: Run as desired user for this machine, DO NOT RUN AS ROOT." && exit

for os in /etc/os-release /usr/lib/os-release; do
	# some POSIX shells exit when trying to source a file that doesn't exist
	[ -f $os ] && . $os && break
done

[ "$ID" = "arch" ] && mng="pacman"
[ "$ID" = "debian" ] && mng="apt"

clear
cat config/extra/welcome.txt

# -----------------------
# Functions for later use
# -----------------------

gen_ssh_keypair() {
    # ssh key already exists
    if [ -e "/home/$usr/.ssh/id_ed25519.pub" ] ; then
        echo
        cat ~/.ssh/id_ed25519.pub
        printf "Copy ~/.ssh/gitkey.pub to git server, press ENTER when done..."
        read -r
	return 0
    fi
    # openssh is not installed
    if ! [ -e "/usr/bin/ssh-keygen" ] ; then
	printf "Missing /usr/bin/ssh-keygen, use http? 	[y/n]: " && read -r usehttp
	[ "$usehttp" = "y" ] && return 2 || exit
    fi
    mkdir -p ~/.ssh
    ssh-keygen -q -t ed25519 -f ~/.ssh/id_ed25519 -N ''
    echo
    cat ~/.ssh/id_ed25519.pub
    printf "Copy ~/.ssh/gitkey.pub to git server, press ENTER when done..."
    read -r
}

clone_dotfiles() {
    printf '\033[1m    %b\033[0m%s\n\n' \
        "Cloning dotfiles repo..."
    [ -d "/home/$usr/.config" ] && mv -f ~/.config ~/.config.old/
    git clone "$GITLAB"/dotfiles ~/.config
}

pkg_manager() {
    if [ -e "/usr/bin/pacman" ] ; then
        printf '\033[1m    %b\033[0m%s\n\n' \
            "Setting up pacman..."
    	sudo cp "$SCRIPTDIR"/config/pkg/pacman.conf /etc/
    	sudo pacman -Syyu
    	sudo pacman -Fy
    	sudo pacman -F Xft.h
        printf '\033[1m    %b\033[0m%s\n\n' \
            "Building yay-bin..."
        # yay AUR helper
        sudo git clone https://aur.archlinux.org/yay-bin.git /tmp/yay-bin
        sudo chown -R "$usr":"$usr" /tmp/yay-bin
        (cd /tmp/yay-bin && makepkg -si)
    fi
}

pkg_download() {
    printf '\033[1m    %b\033[0m%s\n\n' \
        "Downloading packages specified in config/pkg/programs-[pkg_manager].txt"
    if [ -e "/usr/bin/pacman" ] ; then
        sudo pacman -S --needed - < "$SCRIPTDIR"/config/pkg/programs.txt
        # AUR packages
        if [ -e "/usr/bin/yay" ] ; then
            yay -Syu
            yay -S minecraft-launcher game-devices-udev steam # GAMES
            yay -S epson-inkjet-printer-escpr # MY PRINTER
            yay -S --needed ttf-ms-fonts
            yay -S librewolf-bin
            yay -S brave-bin
            [ "$cpu" = "y" ] &&
                yay -S auto-cpufreq
        fi
    fi
}

set_posix_shell() {
    printf '\033[1m    %b\033[0m%s\n\n' \
        "Linking default POSIX shell to dash..."
    sudo /usr/bin/ln -sfT dash /usr/bin/sh || return 2
    [ -e "/usr/bin/pacman" ] && sudo cp "$SCRIPTDIR"/config/pkg/bash-update.hook /usr/share/libalpm/hooks/
}

import_gkey() {
    printf '\033[1m    %b\033[0m%s\n\n' \
        "Importing GPG keypair..."
    sudo /usr/bin/ln -sfT dash /usr/bin/sh || return 2
    echo "Provide the name of the gpg key, eg: 'masterkey'" && read -r nme
    echo "Provide full path for public gpg key, eg: /home/user/keys/publickey.pub" && read -r pub
    echo "Provide full path for private gpg key, eg: /home/user/keys/privatekey.sec" && read -r prv
    gpg --import "$pub"
    gpg --allow-secret-key-import --import "$prv"
    echo "Type: 'trust' and then select a trust level for the imported key..."
    gpg --edit-key "$nme"
}

suckless_de() {
    printf '\033[1m    %b\033[0m%s\n\n' \
        "Building suckless DE..."
    # X11 startup (dwm)
    cp "$SCRIPTDIR"/config/x11/xinitrc ~/.xinitrc
    # If pacman is configured, install instead of compile
    if [ "$pkg" = "y" ] && [ "$mng" = "pacman" ] ; then
        sudo pacman -S dwmblocks-tavo afetch-tavo dmenu-tavo surf-tavo st-tavo dwm-tavo && return 0
    fi
    # Clone needed repos
    mkdir -p ~/.local/src
    : "${repos:=tabbed blocks dmenu surf dwm st}"
    for repo in $repos; do
        ! [ -d "/home/$usr/.local/src/$repo" ] && git clone "$GITLAB"/"$repo" ~/.local/src/"$repo"
    done
    # Build the software
    for dir in ~/.local/src/* ; do
        sudo make install -C "$dir"
    done
}

# --------------------------
# Dialogs for system configs
# --------------------------

printf '\033[1m    %b\033[0m%s\n\n' \
    "Dialogs for setup options..."

# Determine clone method, SSH/HTTP
# If chosen SSH, it will generate an ssh keypair with sensible defaults,
# if there's an error along the process, it will just use http.
printf "Use SSH key to clone repositories? 	[y/n]: " ; read -r key
[ "$key" = "y" ] && gen_ssh_keypair &&
    GITLAB="ssh://git@gitlab.com/tavo-wasd" || GITLAB="https://www.gitlab.com/tavo-wasd"
printf "\nClone with $GITLAB\n\n"

# Basics
printf "Dotfiles (mv .config -> .config.old)	[y/n]: " && read -r dot
printf "bashrc (erase current) (needs .config) 	[y/n]: " && read -r bsh
printf "Package manager configs 		[y/n]: " && read -r pkg
printf "Download packages in config/pkg		[y/n]: " && read -r pks
printf "auto-cpufreq (laptops) 			[y/n]: " && read -r cpu
printf "Networking configs 			[y/n]: " && read -r net
printf "Link /bin/sh to dash			[y/n]: " && read -r psh
[ "$psh" = "y" ] &&
    ! [ -e "/usr/bin/dash" ] && printf "\nWARNING! Missing /usr/bin/dash, unable to link.\n\n" && psh="n"
# Personal
printf "Password vault (ssh clone)		[y/n]: " && read -r pas
printf "Import GPG key 				[y/n]: " && read -r gky
printf "Costa Rica locale 			[y/n]: " && read -r loc
# Extras
printf "Input methods configs (x11) 		[y/n]: " && read -r inp
printf "Echo sudo password 			[y/n]: " && read -r pss
# DE
printf "Build suckless DE (x11) 		[y/n]: " && read -r sck

# --------------------------
# Basic system functionality
# --------------------------

# Proceeding will change basic system functionality, check for confirmation
printf "\nProceeding will change basic system functionality, press ENTER to continue...\n" && read -r NULL

[ "$dot" = "y" ] && clone_dotfiles

[ "$bsh" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Setting up custom bashrc..." &&
    echo "source ~/.config/bashrc" > ~/.bashrc

[ "$pkg" = "y" ] && pkg_manager

[ "$pks" = "y" ] && pkg_download

[ "$net" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Loading network configs..." &&
    sudo cp "$SCRIPTDIR"/config/net/80-ethernet.network /etc/systemd/network/ &&
    sudo cp "$SCRIPTDIR"/config/net/main.conf /etc/iwd/

[ "$psh" = "y" ] && set_posix_shell

[ "$pas" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Cloning password vault..." &&
    git clone "$GITLAB"/passwords ~/.password-store

[ "$gky" = "y" ] && import_gkey

[ "$loc" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Adding Costa Rica locale..." &&
    sudo sed -i "s/#es_CR.UTF-8 UTF-8/es_CR.UTF-8 UTF-8/g" /etc/locale.gen

[ "$inp" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Setting up X11 input methods configs..." &&
    sudo cp "$SCRIPTDIR"/config/x11/95-mouse-accel.conf /etc/X11/xorg.conf.d/ &&
    sudo cp "$SCRIPTDIR"/config/x11/30-touchpad.conf /etc/X11/xorg.conf.d/

[ "$pss" = "y" ] &&
    printf '\033[1m    %b\033[0m%s\n\n' "Enabling 'sudo' password echo..." &&
    echo "Defaults env_reset,pwfeedback" | sudo tee -a /etc/sudoers

# --------------------
# Suckless environment
# --------------------

[ "$sck" = "y" ] && suckless_de

# -----------------------------------
# Only run the following portion once
# -----------------------------------
[ -e "$SCRIPTDIR/.done.tmp" ] && exit
touch "$SCRIPTDIR/.done.tmp"

# ------------------------
# Enable services & locale
# ------------------------
: "${services:=systemd-networkd systemd-resolved usidks2.service auto-cpufreq cups ufw iwd}"
for service in $services; do
    sudo systemctl enable $service
done
# Generate locales enabled in
# /etc/locale.gen
sudo locale-gen

# -----------------
# Theming (TBD @@@)
# -----------------

#yay -S materia-gtk-theme

# Flatpak theming if present
#which flatpak &&
#    mkdir ~/.themes &&
#    sudo cp -r /usr/share/themes/Materia-dark/ ~/.themes/ &&
#    sudo flatpak override --filesystem="$HOME"/.themes/ &&
#    sudo flatpak override --env=GTK_THEME=Materia-dark

# -------------
# Final touches
# -------------

# Give "$usr" permission to manage common groups
sudo usermod -a -G sys,network,scanner,power,libvirt,rfkill,users,video,storage,optical,lp,audio,disk,wheel,root,"$usr" "$usr"

# Allow $usr to reboot & poweroff
echo "$usr ALL = NOPASSWD: /sbin/reboot, /sbin/poweroff" > "$SCRIPTDIR"/config/powerperms
sudo cp "$SCRIPTDIR"/config/powerperms /etc/sudoers.d/

# Emoji
mkdir -p ~/.local/share/chars
cp "$SCRIPTDIR"/config/extra/emojis ~/.local/share/chars/

# Common folders
mkdir -p ~/Pictures/Screenshots
mkdir -p ~/Videos
mkdir -p ~/Downloads
# Clean /home/"$usr"
rm -rf ~/.viminfo
rm -rf ~/.bash_history

echo "Enable GTK theme with lxappearance"

printf '\033[1m\n    %b\033[0m%s\n\n' \
    "Done!"
